# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.find_or_create_by(username:"Marjan",email:"marjanmj3478@gmail.com",password:"Marjan@123")
User.find_or_create_by(username:"Musthafa",email:"musthafa3478@gmail.com",password:"Musthafa@123")
User.find_or_create_by(username:"Athul",email:"atzathul@gmail.com",password:"OnlineKozhi@123")

u = User.create(username:"Vignesh",email:"vignesh@autoattend.com",password:"Vignesh@123")

Comment.create(comments:"good", user_id: u.id)
Comment.create(comments:"bad", user_id: u.id)
Comment.create(comments:"excelent", user_id: u.id)
Comment.create(comments:"poor", user_id: u.id)


u1 = User.create(username:"Ram", email: "ram1112@gmail.com", password: "ram11@33")
u2 = User.create(username:"Sam", email: "sam1121@gmail.com", password: "sam112211")
u3 = User.create(username:"Ram", email: "roy11281@gmail.com", password: "roy@1128")

    [u1, u2, u3].each do |u|
        5.times do
          Comment.create(comments:"very good",rating:"ten", user_id: u.id)
        end
    end


Author.find_or_create_by(firstname:"Marjan",lastname:"Musthafa",email:"marjanmj3478@gmail.com")
Author.find_or_create_by(firstname:"Athul",lastname:"s",email:"athuls@gmail.com")

Author.first.posts.find_or_create_by(title:"First Project",post_content:"Nothing")
Author.first.posts.find_or_create_by(title:"Second Project",post_content:"Blank")

Author.last.posts.find_or_create_by(title:"First Blog",post_content:"Begining")
Author.last.posts.find_or_create_by(title:"Second Blog",post_content:"End")
Post.first.authors.find_or_create_by(firstname:"Shane",lastname:"Watson",email:"waatoo@gmail.com")

#Find all users who have commented "very good".


User.where(comments: Comment.where(comments: "very good"))

User.joins(:comments).where("comments=?","very good").distinct
