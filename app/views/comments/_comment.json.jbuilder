json.extract! comment, :id, :comments, :rating, :created_at, :updated_at
json.url comment_url(comment, format: :json)
