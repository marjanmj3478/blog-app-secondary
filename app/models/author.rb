class Author < ApplicationRecord
  has_and_belongs_to_many :posts
  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :email, uniqueness: true

  before_create :add_record
  before_destroy :delete_record
  before_destroy :delete_posts

  accepts_nested_attributes_for :posts

  def add_record
    ActivityLog.create(description:"Author with email-id #{self.email} is added",operation: :createAuthor)
  end
 
  def delete_record
    ActivityLog.create(description:"Author with email-id #{self.email} is destroyed",operation: :destroyAuthor)
  end

  def delete_posts
    post_ids = posts.pluck(:id)
    #this deletes only the join table entry
    posts.destroy_all
    Post.where(id: post_ids).destroy_all
  end  
end
