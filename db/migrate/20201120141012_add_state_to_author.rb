class AddStateToAuthor < ActiveRecord::Migration[6.0]
  def change
    add_column :authors, :State, :string
  end
end
