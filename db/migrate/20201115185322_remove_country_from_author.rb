class RemoveCountryFromAuthor < ActiveRecord::Migration[6.0]
  def change
    remove_column :authors, :country, :string
  end
end
