class CreateAuthorAndPostTable < ActiveRecord::Migration[6.0]
  def change
    create_table :author_and_post_tables do |t|
      t.belongs_to:author
      t.belongs_to:part
    end
  end
end
