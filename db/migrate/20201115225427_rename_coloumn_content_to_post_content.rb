class RenameColoumnContentToPostContent < ActiveRecord::Migration[6.0]
  def change
    rename_column:posts, :content, :post_content
    change_column:posts, :post_content, :text 
  end
end
