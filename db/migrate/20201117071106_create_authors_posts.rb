class CreateAuthorsPosts < ActiveRecord::Migration[6.0]
  def change
    create_table :authors_posts do |t|
      t.belongs_to :author, null: false, foreign_key: true
      t.belongs_to :post, null: false, foreign_key: true
    end
  end
end
