class Comment < ApplicationRecord
  belongs_to :user
  
  after_create :create_done
  after_destroy :destroy_done

  def create_done
    ActivityLog.create(description: "comment with rating #{self.rating} created", operation: "createComment")
    puts "create done..."
  end

  def destroy_done
    ActivityLog.create(description: "comment with rating #{self.rating} disroyed", operation: "destroyComment")
    puts "destroy done..."
  end

end
