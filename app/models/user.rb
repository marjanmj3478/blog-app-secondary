class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable, :registerable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable, :registerable
  has_many :comments, dependent: :destroy
  validates :email, uniqueness: true
  after_create :create_done
  after_destroy :destroy_done


  def create_done
    ActivityLog.create(description: "user with email_id #{self.email} created", operation: :createUser)
    puts "create done..."
  end

  def destroy_done
    ActivityLog.create(description: "user with email_id #{self.email} desroyed", operation: :destroyUser)
    puts "destroy done..."
  end

end
