class AddColumnToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :dob, :date
    add_column :users, :mob_number, :integer
  end
end
  
