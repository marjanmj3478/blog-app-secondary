class ActivityLog < ApplicationRecord
  enum operation: [:createUser, :destroyUser, :createAuthor, :destroyAuthor, :createPost, :destroyPost, :createComment, :destroyComment]
end
