class Post < ApplicationRecord
  has_and_belongs_to_many :authors
  validates_uniqueness_of :title

  before_create :add_record
  before_destroy :delete_record

  def add_record
    ActivityLog.create(description:"Post with title #{self.title} is added",operation: "createPost")
  end
 
  def delete_record
    ActivityLog.create(description:"Post with title #{self.title} is destroyed",operation: "destroyPost")
  end
  
end
